--1) Script generado para crear base de datos hoy INICIALES_FECHA contenido AdventureWorks2014
CREATE DATABASE MMM_20180320
GO
USE MMM_20180320
GO
--/****** Object:  Table [Production].[WorkOrder]    Script Date: 20/03/2018 18:34:23 ******/
CREATE TABLE [WorkOrder](
	[WorkOrderID] [int] IDENTITY(1,1) PRIMARY KEY CLUSTERED NOT NULL,
	[ProductID] [int] NOT NULL,
	[OrderQty] [int] NOT NULL,
	[StockedQty]  AS (isnull([OrderQty]-[ScrappedQty],(0))),
	[ScrappedQty] [smallint] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[DueDate] [datetime] NOT NULL,
	[ScrapReasonID] [smallint] NULL,
	[ModifiedDate] [datetime] NOT NULL
	)
GO

#2) Sp crear backup tu bd y bd hoy
Import-module sqlps
Get-ChildItem "SQLSERVER:\SQL\W10\DEFAULT\Databases" | where-object {$_.Name -eq "Autoescuela" -or $_.Name -eq "MMM_20180320"} | Backup-SqlDatabase